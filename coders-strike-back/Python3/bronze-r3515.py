import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

used_boost = False
rounds = 0
# game loop
while True:
    rounds += 1
    # next_checkpoint_x: x position of the next check point
    # next_checkpoint_y: y position of the next check point
    # next_checkpoint_dist: distance to the next checkpoint
    # next_checkpoint_angle: angle between your pod orientation and the direction of the next checkpoint
    x, y, next_checkpoint_x, next_checkpoint_y, next_checkpoint_dist, next_checkpoint_angle = [int(i) for i in input().split()]
    opponent_x, opponent_y = [int(i) for i in input().split()]

    # 2.2, 5, no dist
    # if next_checkpoint_dist > 2000:
    #     angle = 100
    # else:
    angle = int((1-pow(math.fabs(next_checkpoint_angle)/180, 2)) * 80)
    # dist = int(min(100*next_checkpoint_dist/1000, 100))
    if angle > 70:
        angle = 80
    thrust = angle + 20 #max(angle, 20)

    if rounds >= 100 and not used_boost and next_checkpoint_angle < 5 and next_checkpoint_dist > 6000:
        thrust = "BOOST"
        used_boost = True

    print("Dist " + str(next_checkpoint_dist) + " Angle " + str(next_checkpoint_angle) + " Thrust " +  str(thrust) + " B:" + str(used_boost), file=sys.stderr)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)


    # You have to output the target position
    # followed by the power (0 <= thrust <= 100)
    # i.e.: "x y thrust"
    print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " " + str(thrust))
